define(function(require) {
    var Model = require('spnd/models/models').Model;
    var spnd = {};
    spnd['m'] = {
        'Model':Model,
        'registry':require('spnd/models/registry'),
        'validators':require('spnd/models/validators')
    };
    spnd['s'] = require('spnd/server');
    spnd['b'] = {
        'Binder': require('spnd/binding/binder'),
        'Handlers': require('spnd/binding/handlers')
    };
    spnd['v'] = {
        'View':require('spnd/views/view')
    };
    spnd['t'] = require('spnd/views/templates');
    spnd['$'] = null;
    return spnd;
});
