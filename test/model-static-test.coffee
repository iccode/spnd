define ['spnd'], (spnd)->

    Model = spnd.m.Model

    TestModel = Model "TestModel",
        properties: {
            'id':''
            'label':''
        }

    TestSubClass = Model "TestSubClass", TestModel,
        properties:
            "subprop":''
            'comp':->"test+"+@id


    describe 'Model structure', ->
        it 'supports constructors', ->
            z = new TestModel({id:5})
            expect(z.id).toEqual(5)

        it 'works with no object', ->
            z = new TestModel()
            expect(z.id).toEqual(null)


    describe 'Model subclassing', ->

        it 'inherits fields', ->
            m = new TestSubClass id:5, subprop:'test'
            expect(m.id).toEqual(5)
            expect(m.subprop).toEqual('test')

        it 'supports computed properties', ->
            m = new TestSubClass id:5, subprop:'test'
            expect(m.comp).toEqual('test+5')

