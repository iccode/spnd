define ['jquery','spnd/binding/expr'], ($, expr)->

    body = $(document.body)

    describe "Binding parser", ->

        it "supports window", ->
            arg = """ text: "hello" """

            p = expr.parseBinding(arg)

