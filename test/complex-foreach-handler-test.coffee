define ['jquery','spnd'], ($, spnd)->

    Model = spnd.m.Model
    body = $(document.body)

    makeTest = (html) ->
        $('#test').remove()
        test_html = "<div id='test'></div>"
        body.append(test_html)
        return $('#test').html(html)

    Phone = Model 'Phone',
        properties:
            'tag':''
            'label':''
            'number':''

    Customer = Model "Customer",
        properties: {
            'id':''
            'label':''
            'bool':'',
            'phones':'Phones[]',
            'sub':''
        }

    describe "Foreach with complex object", ->
        it "works with child-bindings", ->
            makeTest("""
                     <div data-bind='foreach: phones'>
                        <input class='ph' type='text' data-bind='value: number'>
                    </div>
                     """)

            window.c = new Customer()

            window.b = new spnd.b.Binder(c, '#test', true)
            c.phones.push(new Phone(number: '1'))
            c.phones.push(new Phone(number: '2'))
            c.phones.push(new Phone(number: '3'))

            expect($('#test .ph').length).toBe(3)

            $('#test .ph')[1].focus()
            $('#test .ph')[1].value = 'i-set-that'
            $($('#test .ph')[1]).change()

            expect(c.phones[1].number).toBe('i-set-that')
            c.phones[0].number = ":)"
            c.phones[2].number = ":("

            for i in [1..10]
                c.phones.push(new Phone(number: i+"fff"))

            window.b.dispose()
