tests = Object.keys(window.__karma__.files).filter((file) ->
      return /test\.js$/.test(file);
);

requirejs.config({
    baseUrl: '/base',
    paths: {
        'jquery':'bower_components/jquery/jquery'
    }

    deps: tests,
    callback: window.__karma__.start
})