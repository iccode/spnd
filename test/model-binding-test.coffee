define ['jquery','spnd'], ($, spnd)->

    Model = spnd.m.Model
    body = $(document.body)

    makeTest = (html) ->
        $('#test').remove()
        test_html = "<div id='test'></div>"
        body.append(test_html)
        return $('#test').html(html)

    TestModel = Model "TestModel",
        properties: {
            'id':''
            'label':''
            'bool':''
        }

    TestSubClass = Model "TestSubClass", TestModel,
        properties:
            "subprop":''
            'comp':->"test+"+@id
            'items':'[]'

    describe "Binding behavior", ->

        it "binds simple value", ->
            makeTest().html("""
                    <label id='l' data-bind='text: comp'></label>
                    <input id='i' data-bind='value: id'>
                    """)

            obj = new TestSubClass('id':5)
            b = new spnd.b.Binder(obj, '#test', true)
            expect($('#i').val()).toEqual("5")
            expect($('#l').text()).toEqual("test+5")
            b.dispose()


        it "binds select element", ->
            makeTest("""
                        <select id='i' data-bind='value: id'>
                            <option value=''></option>
                            <option value='5'>Correct</option>
                        </select>
                    """)

            obj = new TestSubClass('id':5)
            b = new spnd.b.Binder(obj, '#test', true)
            expect($('#i').val()).toEqual("5")
            console.debug "--------------------------"
            b.dispose()
            console.debug "=========================="


    describe "Binding behavior for checkbox", ->
        it "responds to checkbox", ->
            makeTest("""
                     <input id='i' type='checkbox' data-bind='checked: bool'>
                     """)

            obj = new TestSubClass('id':5, bool:true);
            b = new spnd.b.Binder(obj, '#test', true)

            expect($('#i').val()).toEqual('on')
            $('#i').click()
            expect($('#i')[0].checked).toEqual(false)
            expect(obj.bool).toBe(false)
            $('#i').click()
            expect(obj.bool).toBe(true)

            b.dispose()

    describe "Binding array", ->

        it "binds array", ->
            makeTest("""
                    <ul id='u' data-bind='foreach: items'>
                        <li data-bind='text: this'></li>
                    </ul>
                    """)

            obj = new TestSubClass(items:[1,2,3,4])
            b = new spnd.b.Binder(obj, '#test', true)
            expect($('#u').children().length).toBe(4)

            obj.items.push(5)
            expect($('#u').children().length).toBe(5)
            b.dispose()


    describe "Foreach handler", ->

        it "supports multiple templates", ->
            makeTest("""
                     <div id='a' data-bind='foreach: a'><span data-bind='text: this.$this' class='a'></span></div>
                     <div id='b' data-bind='foreach: b'><span data-bind='text: this.$this' class='b'></span></div>
                     """)


            window.__DEBUG=true

            z = {a:[1,2], b:[4,5]}
            b = new spnd.b.Binder(z, '#test', true)

            expect($('#a').children().length).toBe(z.a.length)
            expect($('#b').children().length).toBe(z.b.length)

            z.b.push(10)
            expect($('#b').children().length).toBe(z.b.length)
            expect($('#a').children().length).toBe(z.a.length)

            z.a.push(1)
            expect($('#a').children().length).toBe(z.a.length)
            expect($('#b').children().length).toBe(z.b.length)

            z.a = []
            expect($('#a').children().length).toBe(z.a.length)
            expect($('#b').children().length).toBe(z.b.length)

            z.a.push('z')
            expect($('#a').children().length).toBe(z.a.length)
            expect($('#b').children().length).toBe(z.b.length)
            b.dispose()

        it "supports successive bindings", ->
            makeTest("""
                     <div id='a' data-bind='foreach: a'><span data-bind='text: this.$this' class='a'></span></div>
                     <div id='b' data-bind='foreach: b'><span data-bind='text: this.$this' class='b'></span></div>
                     """)


            window.__DEBUG=true

            z = {a:[1,2], b:[4,5]}
            b = new spnd.b.Binder(z, '#test', true)

            expect($('#a').children().length).toBe(z.a.length)
            console.log($('#a')[0].outerHTML)
            expect($('#b').children().length).toBe(z.b.length)
            b.dispose()
            b = new spnd.b.Binder(z, '#test', true)

            expect($('#a').children().length).toBe(z.a.length)
            expect($('#b').children().length).toBe(z.b.length)
            b.dispose()


    describe "Regression Tests", ->
        it "doesn't confuse string for array", ->
            makeTest("<div id='a' data-bind='text: phone'></div>")
            z = {'phone':'z'}
            b = new spnd.b.Binder(z, '#test', true)

            expect($('#a').text()).toBe('z')

            z.phone = 'abc123'
            expect($('#a').text()).toBe('abc123')
            b.dispose()

    describe "Binding complex objects", ->
        it "resolves attributes correctly", ->
            makeTest("<span id='a' data-bind='text: a.b.c'></span>")
            z = {
                'a':{
                    'b':{
                        'c': 'Test'
                    }
                }
            }
            b = new spnd.b.Binder(z, '#test', true)
            expect($('#a').text()).toBe('Test')
            b.dispose()

    describe "With handler", ->
        it "binds with 'with' handler", ->
            makeTest("""
                     <div id='t' data-bind='with: customer'>
                     <span id='a' data-bind='text: z'></span>
                     </div>
                     """)

            obj = {
                'customer': {
                    'z': 'hello'
                }
            }
            b = new spnd.b.Binder(obj, '#test', true)

            expect($('#a').text()).toBe('hello')

            obj.customer.z = 'world'

            expect($('#a').text()).toBe('world')
            b.dispose()


        it "binds two way", ->
            makeTest("""
                     <div id='t' data-bind='with: customer'>
                     <input type='checkbox' id='c' data-bind='checked: z'></span>
                     </div>
                     """)


            obj = {
                'customer': {
                    'z': true
                }
            }
            b = new spnd.b.Binder(obj, '#test', true)
            expect($('#c')[0].checked).toBe(true)
            $('#c').click()
            expect($('#c')[0].checked).toBe(false)
            expect(obj.customer.z).toBe(false)
            b.dispose()

    describe "Click Handler", ->
        it "responds to clicks", ->
            makeTest("""
                     <div id='t' >
                     <span id='a' data-bind='click: customer.z'></span>
                     <span id='b' data-bind='click: customer.f'></span>
                     </div>
                     """)

            o = {
                t:0,
                'customer':{
                    'z': -> o.t = o.t + 2
                    'f': -> o.t = o.t - 2
                }
            }

            b = new spnd.b.Binder(o, '#test', true)

            $('#a').click()
            expect(o.t).toBe(2)
            $('#b').click()
            expect(o.t).toBe(0)
            b.dispose()

        it "is disposed correctly", ->
            makeTest("""
                     <div id='t' >
                     <span id='a' data-bind='click: customer.z'></span>
                     </div>
                     """)

            o = {
                t:0,
                'customer':{
                    'z': ->
                        o.t++
                }
            }

            b = new spnd.b.Binder(o, '#test', true)
            $('#a').click()
            expect(o.t).toBe(1)
            b.dispose('#test')

            #$('#a').off('click.binder')
            $('#a').click()
            expect(o.t).toBe(1)


