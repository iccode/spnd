define [
    "./../server",
    './registry',
    './validators',
    '../utils',
    './_templates'
],
(srv, registry, validators, utils, templates)->
    "use strict"

    {CLASS_TEMPLATE, RW_PROPERTY, PROP_SETTER, RO_PROPERTY_FN} = templates

    __modelReplacer = (key, value)->
        return undefined  if key[0] == '$' or key[0] == '_'
        return null if typeof value == "object" and value != null and Object.keys(value).length == 0
        return (a for a in value) if value?.__artype__? #we need special provision for NotifiableArray
        return value

    __modelWithNullReplacer = (key, value)->
        return undefined  if key[0] == '$' or key[0] == '_'
        return value or null

    make_getterFn = (name, prop)->
        return new Function "a", "return this.#{prop or '__model__.ko'}['#{name}']();"

    make_setterFn = (name)->
        return new Function "newval", PROP_SETTER(name)

    typeOf = (val)->
        s = typeof val
        if s == 'object'
            if val
                if val instanceof Array
                    return 'array'
            else
                return 'null'

        return s

    _EMPTY_ = Object.create(null)

    VALIDATORS = {
        'required': (v)->
            v != null
    }

    NotifiableArray = (array, __self, name, type) ->
        _this = new Array()
        _this.__artype__ = type
        if not type?
            _this.push(i) for i in array
        else
            for i in array
                t = new type(i)
                t.markLoaded() if t.markLoaded
                _this.push(t)

        _this.__push = _this.push;
        _this.push = (item)->
            _this.__push(item)
            if typeof(window.__monitor_set__) == "function" and typeof __self['__monitor__'] == "object"
                window.__monitor_set__(__self['__monitor__'][name])

        _this.empty = -> _this.length = 0

        return _this;


    class ModelBase
        constructor: ()->
            @__model__ = {}
            @__validators__ = []
            @key = null

            Object.defineProperty(@, "isValid", {
                configurable: false
                get: @.__isValid__
            })

        __bind__: (obj)=>
            ###
              Populates the model properties with the values of an object.
              It's a simple mapping where property obj.a goes to instance.a.
            ###
            @__url__ = obj.__url__ if obj?.__url__?
            return if not obj?
            tko = @__model__
            for k in Object.keys(obj)

                # first check if the property is a model property
                if tko[k]
                    # if we get a null for an array value, keep the empty array
                    continue if obj[k]==null and tko[k].__artype__

                    # if we got an array, make it a notifiable array
                    if obj[k] instanceof Array
                        tko[k] = NotifiableArray(obj[k], @, k,
                          registry.get(tko[k].__artype__))
                        continue

                    # no special handling
                    tko[k] = obj[k]
                    continue

                # if not, just copy
                @[k] = obj[k]


        validate: ()=>
            [v.getErrorMessage() for v in @__validators__ when not v.validate(@)]

        __isValid__: => not v.validate(@) for v in @__validators__

        'strip': (withNulls)=> JSON.parse(@['asJSON'](withNulls))

        'asJSON': (withNulls)=>
            s = JSON.stringify(@, if withNulls then __modelWithNullReplacer else __modelReplacer)
            s.replace /[\u007f-\uffff]/g, (c)-> '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4)

        'markLoaded': =>
            Object.defineProperty(@, '__loaded__', {
                configurable: false,
                value: true,
                writable: false,
                enumerable: false
            })



    #makes the code that comprises a property
    #Actually generated the proper ko observables and register the properties
    __makeProperty = (name, property)->
        if property.isArray
            return "\n__tm['#{name}']=ns.NotifiableArray([], _self, '#{name}', '#{property.type}');"

        else if typeOf(property) == "string"
            return __makeProperty(name, {}) if property.length == 0

            args = {}
            if property[property.length - 1] == "!"
                args.isRequired = true
                args.type = property.slice(1)

            else if property.slice(-2) == "[]"
                args.isArray = true
                args.type = (args.type or property).slice(0, -2)

            else
                cls = registry.get(property)
                args.type = cls

            return __makeProperty(name, args)

        else if typeOf(property) == "function"
            return "\n__tm['#{name}']=__bind(#{property.toString()},_self);\n"

        else if typeOf(property) == "object"
            code = ""
            code += "\n__tm['#{name}']=_EMPTY_;";
            if property.isRequired
                code += "\n__v.push(validators.new('required','#{name}'));"

            return code

        return "//name: #{name}"



    # We create an actual "class " definition, by generating the code of the
    # constructor class. The basic mechanism is copied by how classes work in
    # coffeescript and we augment it with our stuff
    __makeClassMaker = (className, prototype)->
        propFields = []
        propDefs = []
        for propname in Object.keys(prototype.properties)
            prop = prototype.properties[propname]
            propFields.push(__makeProperty(propname, prop))
            propDefs.push((if typeOf(prop) == 'function' then RO_PROPERTY_FN else RW_PROPERTY)(propname))

        methodDefs = []
        for methodname in Object.keys(prototype.methods or {})
            methodDefs.push("this.#{methodname}=__bind(this.#{methodname},_self);")

        fn = CLASS_TEMPLATE(
            className: className,
            propDefs: propDefs.join("\n,\n"),
            propFields: propFields.join("\n"),
            boundMethods: methodDefs.join("\n;\n")
        )

        new Function('__parent__', 'validators', 'clsProto', '_EMPTY_', 'ns', fn)


    Model = (name, parent, prototype)->
        if not prototype?
            prototype = parent
            parent = ModelBase

        clsMaker = __makeClassMaker(name, prototype)
        cls = clsMaker(parent, validators, prototype, _EMPTY_, {
            NotifiableArray: NotifiableArray
        })

        cls.prototype[k] = m for k, m of prototype.methods
        cls.__name__ = name
        cls.__baseURL__ = prototype['__baseURL__'] if prototype['__baseURL__']
        cls.prototype['__baseURL__'] = prototype['__baseURL__'] if prototype['__baseURL__']
        registry.register(cls, name)
        return cls

    return {
        'ModelBase': ModelBase,
        'Model': Model
    }

