define ()->
    __VALIDATORS__ = {}

    exports = {
        'register': (validator)->
            __VALIDATORS__[validator['__name__']] = validator
        'new': (validator, args...)->return new __VALIDATORS__[validator](args...)
    }

    class ValidatorBase
        constructor: (@field, @errorMessageTemplate)->
            if not @errorMessageTemplate
                @errorMessageTemplate = "Error in #{@field}"

        getErrorMessage: ()=>
            return @errorMessage if @errorMessage
            matches = @errorMessageTemplate.match(/(.*)/)
            if matches.length > 1
                field = matches[1]
                return @errorMessageTemplate.replace("{{#{field}}}", @[field])

            return "#{@__name__} failed"

        validate: ->
            throw new Error("Not implemented")

    # --------------------------
    # required validator
    class RequiredValidator extends ValidatorBase
        @['__name__'] = 'required'

        constructor: (@field, @errorMessage)->
            super(@field, "{{field}}: Field is required")

        validate: (ctx)=>
            ctx[@field]?.length > 0

    exports.register(RequiredValidator)

    # --------------------------
    # regex validator
    class RegexValidator extends ValidatorBase
        @['__name__'] = 'rx'

        constructor: (@field, errorMessage, @regex)->
            super(@field, "{{field}}: Regex failed")
            @rx = new RegExp(@regex)

        validate: (ctx)=>
            @rx.test(ctx[@field])

    exports.register(RegexValidator)


    return exports
