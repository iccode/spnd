define ['./../server', './models'], (srv, models)->
    class Registry
        constructor: ()->
            @__MODELS = {}
            @['settings'] = {
                'root': '/res/'
            }

        __predToURL : (predicates, ordering)->
            url = []
            for p in predicates
                url[url.length] = "#{p.f}=#{p.v}"

            if ordering
                url[url.length] = "_ordering=#{ordering.d}#{ordering.f}"

            return url.join("&")

        __getURL : (model, usage, predicates, ordering)=>
            if model.resURL?
                return @['settings']['root']+model.resURL

            if typeof(model.__url__) == "string"
                return @['settings']['root'] + model.__url__

            if typeof(model.__url__) == 'fuction'
                url = model.__url__(usage, predicates, ordering)

            if typeof(model.__baseURL__) == 'string'
                r = model.__baseURL__
                url = "#{r}/#{predicates}" if usage=="id"
                url = "#{r}?#{@__predToURL(predicates, ordering)}" if usage=="search"
                url = "#{r}" if usage=="saving"

            return @['settings']['root']+url


        'register': (model, name)=>
            if not name?
                name = model.prototype['__name__']

            return if @__MODELS[name]
            @__MODELS[name] = model
            @[name] = model

        'get': (model)=>
            @__MODELS[model]

        'from': (model)=>
            if typeof(model) == "string"
                return new RegistryQuery(@__MODELS[model], @)
            else
                name = model.prototype['__name__']
                return new RegistryQuery(@__MODELS[name], @)

        'save': (model)=>
            $.ajax
                'url': @__getURL(model, 'saving')
                'dataType':'json',
                'contentType':'application/json',
                'data': model.asJSON(),
                'type': if model.__loaded__ then 'PUT' else 'POST',
                'processData':false

        'delete': (model)=>
            $.ajax
                'url': @__getURL(model, 'saving')
                'dataType':'json',
                'contentType':'application/json',
                'type': 'DELETE'

    REGISTRY = new Registry()

    class RegistryQuery
        constructor: (model, @__registry)->
            @__model = model
            @__predicates = []
            @__ordering = null

        where: (predicates, ordering)=>
            if predicates
                [].apply(@__predicates, predicates...)

            @__ordering = ordering if ordering
            return @

        clear: ()=>
            @__predicates = []

        eq: (field, val)=>
            @__predicates.push({'f': field, 'v': val, 'op': '='})
            @

        neq: (field, val)=>
            @__predicates.push({'f': field, 'v': val, 'op': '!='})
            @

        gt: (field, val)=>
            @__predicates.push({'f': field, 'v': val, 'op': '>'})
            @

        lt: (field, val)=>
            @__predicates.push({'f': field, 'v': val, 'op': '<'})
            @

        gte: (field, val)=>
            @__predicates.push({'f': field, 'v': val, 'op': '>='})
            @

        lte: (field, val)=>
            @__predicates.push({'f': field, 'v': val, 'op': '<='})
            @

        order: () =>
            if arguments.length == 1
                @__ordering = {'fs': [arguments[0]], 'd': ''}
            if arguments.length == 2
                [field, last] = arguments
                if last.toLocaleLowerCase() == "desc"
                    @__ordering = {"f": field, "d": '-'}
                else
                    @__ordering = {"f": field, "d": ""}

            return @

        all: ()=>
            p = $.ajax
                'url':@__registry.__getURL(@__model, 'search', @__predicates, @__ordering)
                'dataType':'json',
                'contentType':'application/json',

            cls = @__model
            return p.then(
                (res)->
                    ret = (new cls(r) for r in res)
                    m.markLoaded() for m in ret
                    return ret
            )

        id: (value)=>
            d = $.Deferred()
            p = $.ajax
                'url':@__registry.__getURL(@__model, 'id', value)+"?_view=full"
                'dataType':'json',
                'contentType':'application/json',

            cls = @__model
            p.done (res)->
                ret = new cls(res[0])
                Object.defineProperty(ret, '__loaded__', {
                    configurable: false,
                    value: true,
                    writable: false,
                    enumerable: false
                })

                d.resolve(ret)

            p.fail ->
                d.reject()

            return d.promise()

    return REGISTRY
