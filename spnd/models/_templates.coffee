define ['./doU'], (doU)->
    RW_PROPERTY = """
                  '@': {
                  get: function() {

                    if ( typeof window.__monitor_get__ === "function" &&
                         typeof _self.__monitor__ === "object"
                        ) 
                        window.__monitor_get__(_self.__monitor__.@);
                        

                    var v = _self.__model__.@;
                    try {
                        if ( typeof(v) == "object" && !Array.isArray(v)  &&
                             Object.keys(v).length == 0
                           ) return null;
                             
                    }catch(e){}

                    return v;
                  },

                  set: function(nv) {
                    
                    if (Array.isArray(nv)) {
                        nv = new ns.NotifiableArray(nv, _self.__model__.@.__artype__);
                        }

                    _self.__model__.@ =nv;
                    if ( typeof window.__monitor_set__ === "function" &&
                         typeof _self['__monitor__'] === "object"
                       ) {
                        window.__monitor_set__(_self.__monitor__.@);
                        }

                  },

                  configurable: false,
                  enumerable: true
                  }
                  """

    RO_PROPERTY = """
                  {{=it}}: {
                  get: function() {
                  if (typeof window.__monitor_get__ === "function" &&
                  typeof _self['__monitor__'] === "object"
                  ) {
                  window.__monitor_get__(_self['__monitor__']['{{=it}}']);
                  }
                  return _self.__model__.{{=it}};
                  },
                  configurable:false
                  }
                  """


    RO_PROPERTY_FN = """
                     {{=it}}: {
                     get: function() {
                     return _self.__model__.{{=it}}();
                     },
                     configurable:false
                     }
                     """

    PROP_SETTER = """
          var _m = _self.__model__;
          if (!_self.isDirty){_m.dirty = _m['{{=it}}']===newval; }
          _m['{{=it}}'] = newval;
    """


    #
    # Defines a template for class maker. This describes a function that
    # generates a class.
    #

    CLASS_TEMPLATE = """
     var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
         __hasProp = {}.hasOwnProperty,
         __extends = function(child, parent) {
            for (var key in parent) {
                if (__hasProp.call(parent, key)) child[key] = parent[key];
            }

            function ctor() { this.constructor = child; }
            ctor.prototype = parent.prototype;
            child.prototype = new ctor;
            child.__super__ = parent.prototype;

            return child;\n
         };

     function {{=it.className}} (dict) {
         var __tm = null,
             __dp=Object.defineProperty,
             _self = this,
             __v=null;

         {{=it.className}}.__super__.constructor.apply(this, arguments);

         __v = _self.__validators__;
         __tm = this['__model__'];

         {{=it.propFields}}


         Object.defineProperties(_self, {
            {{=it.propDefs}}
         });

         {{=it.boundMethods}}

         if (arguments.length===1) this.__bind__(arguments[0]);
         if (this.__init__) this.__init__.apply(_self, arguments);



     }

     __extends({{=it.className}}, __parent__);

     {{=it.className}}.prototype.__prototype__ = clsProto;

     return {{=it.className}}
     """;

    return {
        CLASS_TEMPLATE: doU.template(CLASS_TEMPLATE),
        RW_PROPERTY: ((r)-> 
            (RW_PROPERTY+"").replace(/\@/g,r) 
        ),
        PROP_SETTER: doU.template(PROP_SETTER),
        RO_PROPERTY_FN: doU.template(RO_PROPERTY_FN)
    }
