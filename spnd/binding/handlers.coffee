define ()->
    exports = {}

    HANDLERS = {}

    exports['registerHandler'] = (key, prototype)->
        HANDLERS[key] = {
            'isMutator': prototype.isMutator or false
            'init': (node, ctx, valueAccessor)->
                prototype.init(node, ctx, valueAccessor)
            'update': (node, ctx, valueAccessor)->
                prototype.update(node, ctx, valueAccessor)
        }

    exports['applyHandler'] = (handler_name, action, node, ctx, expression)->
        handler = HANDLERS[handler_name]
        throw new Error("Handler #{handler_name} is unknown") if not handler?


        $n = $(node)
        accessor = ()-> expression.fn.call(ctx, ctx.$this)

        if action == "init"
            try
                handler.init($n, ctx, accessor)
            catch error
                console.log("Couldn't apply handler #{handler_name} for #{node.outerHTML}")
                console.log(error)

        else if action == "update"
            handler.update($n, ctx, accessor)

    ## ==================================================
    ## Default handlers
    ## ==================================================

    #text handler
    exports.registerHandler "text", {
        init: (node, ctx, valueAccessor)->
            $(node).text(valueAccessor())

        update: (node, ctx, valueAccessor)->
            $(node).text(valueAccessor())
    }

    #html handler
    exports.registerHandler "html", {
        init: (node, ctx, valueAccessor)->
            $(node).html(valueAccessor())

        update: (node, ctx, valueAccessor)->
            $(node).html(valueAccessor())
    }

    #value handler
    exports.registerHandler "value", {
        isMutator: true

        init: (node, ctx, valueAccessor)->
            $(node).val(valueAccessor())

        update: (node, ctx, valueAccessor)->
            $(node).val(valueAccessor());
    }

    #visible handler
    exports.registerHandler "visible", {
        init: (node, ctx, valueAccessor)->
            show = valueAccessor()
            if show
                $(node).show()
            else
                $(node).hide()

        update: (node, ctx, valueAccessor)->
            show = valueAccessor()
            if show
                $(node).show()
            else
                $(node).hide()
    }

    #style handler
    exports.registerHandler "style", {
        init: (node, ctx, valueAccessor)->
            $(node).css(valueAccessor())

        update: (node, ctx, valueAccessor)->
            $(node).css(valueAccessor())
    }

    exports.registerHandler "checked", {
        init: (node, ctx, valueAccessor)->
            $(node)[0].checked = valueAccessor()

        update: (node, ctx, valueAccessor)->
            $(node)[0].checked = valueAccessor()
    }

    #attr handler
    exports.registerHandler "attr", {
        init: (node, ctx, valueAccessor)->
            $(node).attr(valueAccessor())

        update: (node, ctx, valueAccessor)->
            $(node).attr(valueAccessor())
    }

    #with handler
    exports.registerHandler "with", {
        init: (node, ctx, valueAccessor)->
            subctx = ctx.makeContext(valueAccessor())
            n = node[0].firstChild
            while n
                subctx.binder.__bindNode(n, subctx) if n.nodeType == 1
                n = n.nextSibling

        update: (node, ctx, valueAccessor)->

    }

    #foreach handler
    #TODO: revisit that. far from optimal
    exports.registerHandler "foreach", {
        init: (node, ctx, valueAccessor)->
            templateId = Math.random().toString(12).substring(6).toString()
            ch = node.children().detach()
            if ch.length > 1
                ch = $("<div></div>").append(ch)
            ch = ch[0].outerHTML

            ctx[templateId] = ch #node.children().detach()
            node[0].dataset._template = templateId

            for v in valueAccessor()
                subctx = ctx.makeContext(v)
                n = $(ch).appendTo(node)
                subctx.binder.__bindNode(n[0],subctx)


        update: (node, ctx, valueAccessor)->
            node.children().each((i, e)->
                ctx.binder.__disposeElementBinding(e) if e.dataset._ctx!=ctx.id+""
                $(e).remove()
            )
            $ch = ctx[node[0].dataset._template]

            for v in valueAccessor()
                subctx = ctx.makeContext(v)
                n = $($ch).appendTo(node)
                subctx.binder.__bindNode(n[0],subctx)

            ctx.binder.gc()
    }

    #TODO: check the parsing/compiling of the css object
    #we need to distinguish between actual strings (maybe keys can only be strings)
    #and actual identifiers. Right now, keys are treated as strings
    exports.registerHandler "css", {
        init: (node, ctx, valueAccessor)->
            classes = valueAccessor()
            for klass in Object.keys(classes)
                cssClass = if klass[0] == "'" or klass[0] == '"' then klass.slice(1, -1) else klass
                if classes[klass]
                    $(node).addClass(cssClass)
                else
                    $(node).removeClass(cssClass)

        update: (node, ctx, valueAccessor) ->
            classes = valueAccessor()
            for klass in Object.keys(classes)
                cssClass = if klass[0] == "'" or klass[0] == '"' then klass.slice(1, -1) else klass
                if classes[klass]
                    $(node).addClass(cssClass)
                else
                    $(node).removeClass(cssClass)
    }

    #TODO: add support for bindgins in result
    exports.registerHandler "template", {
        init: (node, ctx, valueAccessor)->
            val = valueAccessor()
            template = ctx['binder']['settings']['template'](val.name)
            if template
                $(node).html(template(val.data))

        update: (node, ctx, valueAccessor)->
            val = valueAccessor()
            template = ctx['binder']['settings']['template'](val.name)
            if template
                $(node).html(template(val.data))
    }

    exports.registerHandler "click", {
        init: (node, ctx, valueAccessor)->
            v = valueAccessor()
            id = Math.random().toString(12).substring(6)
            node.addClass(id)
            do (fun=v)->
                ctx.binder.$e.on('click.binder', ".#{id}",
                    (evt)->fun.apply(ctx.$this, arguments))

        update: (node, ctx, valueAccessor)->
    }

    return exports
