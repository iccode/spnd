define ['./acorn'], (acorn)->
    $ = window.jQuery
    ns = {}
    EXPRESSIONS = 0
    window.__BINDINGS__ = []
    window.__BINDINGS_CACHE__ = {}

    # =======================================
    # Binding expression handling and parsing
    #
    # It may seem funny to parse javascript just to compile it again to
    # javascript but the reason is twofold.
    #
    # 1) We get an AST we can valiate against
    # 2) We can manipulate the AST to suit our needs regarding how binding
    #    is working.
    #
    # When we get the ast we can validate that the objects/functions actually
    # exist in the given binding root, so we can notify the user that he is
    # missing something.
    # =======================================

    __makeBindingObj = (ast, fn, wfn)->
        return Object.create(null, {
            'id':
                enumerable: false, writable: false, configurable: false, value: ++EXPRESSIONS
            'ast':
                enumerable: false, writable: false, configurable: false, value: ast
            'fn':
                enumerable: false, writable: false, configurable: false, value: fn
            'wfn':
                enumerable: false, writable: false, configurable: false, value: wfn
        })

    # the actual binding parser
    __bindingParser = (binding)->
        old = binding

        binding = "text: #{binding}" if ":" not in binding
        binding = "expr={#{binding}};"

        strlen = binding.length

        try
            parsed = acorn.parse(binding)
            expr = parsed.body[0].expression.right
            if not expr.type == "ObjectExression"
                throw new Error("Wrong syntax expression in #{old}")

            return expr.properties
        catch error
            if error.loc
                ln = ( if error.loc == a then "^" else " " for a in [0..strlen]).join("")
                throw new Error(binding + "\n" + ln)
            else
                throw error

    # compiles back an ast-node to javascript code.
    __compileNode = (arg1, node)->
        if node.type == "ObjectExpression"
            code = "{"
            objCode = []
            for pnode in node.properties
                c = "'#{pnode.key.value or pnode.key.name}':#{__compileNode(arg1,
                  pnode.value)}"
                objCode.push(c)

            code += objCode.join(",")
            code += "}"
            return code

        else if node.type == "Identifier"
            return "#{arg1}.#{node.name}"

        else if node.type == "ConditionalExpression"
            return __compileNode(arg1, node.test) + "?" + __compileNode(arg1,
              node.consequent) + ":" + __compileNode(arg1, node.alternate)

        else if node.type == "BinaryExpression"
            return __compileNode(arg1,
              node.left) + " " + node.operator + __compileNode(arg1,
              node.right)

        else if node.type == "UnaryExpression"
            return "#{node.operator}#{__compileNode(arg1, node.argument)}"

        else if node.type == "MemberExpression"
            if node.object.type == "ThisExpression"
                return "this.#{node.property.name}"
            if node.object.type == "MemberExpression"
                return __compileNode(arg1, node.object)+".#{node.property.name}"

            return "#{arg1}.#{node.object.name}.#{node.property.name}"

        else if node.type == "Literal"
            if node.raw
                return node.raw
            else
                return node.value

        else if node.type == "ThisExpression"
            return "this"

        else if node.type == "CallExpression"
            return __compileNode(arg1, node.callee) + "(#{(__compileNode(arg1,
              arg_node) for arg_node in node.arguments).join(',')})"

        return "//something wrong for expression #{node.type}"

    # compiles the actual ast tree of the binding expression
    # to a function that can be called and returns the arguments
    ns.makeAccessor = (root, ast)->
        node = __compileNode('$this', ast);
        fn_code = "return #{node};"
        try
            return new Function('$this', fn_code)
        catch error
            console.log "[expr] Error while creating fn for #{fn_code}"
            return null

    # compiles the actual ast tree to a function that mutates
    # the expression
    ns.makeMutator = (root, ast)->
        try
            MUTABLE_EXPRESSIONS = ['MemberExpression', 'Identifier']
            if MUTABLE_EXPRESSIONS.indexOf(ast.type) < 0
                return ns.makeAccessor(root, ast) #we can't make a mutator

            node = __compileNode('root', ast);
            fn_code = "#{node} = nv;return #{node}"
            return new Function('root', 'nv', fn_code)
        catch error
            console.error("[expr] Error while making mutator #{fn_code}")
            return null

    # parses the binding expression to a dictionary of
    # binding handlers to ast of the binding expressions
    ns.parseBinding = (str)->

        #search if we have already cached it
        cachedBindingMark = __BINDINGS_CACHE__[str]
        if cachedBindingMark
            cachedBinding = {}
            for k in Object.keys(cachedBindingMark)
                exprIdx = cachedBindingMark[k]
                if not __BINDINGS__[exprIdx] #if miss an expression, invalidate everything
                    cachedBinding = null
                    break
                cachedBinding[k] = __BINDINGS__[exprIdx]

            #if it was disposed, clear the cache mark
            if cachedBinding == null
                delete __BINDINGS_CACHE__[str]
            else
                return cachedBinding

        try
            props = __bindingParser(str)
            bindings = {}
            cache_mark = {}
            for p in props
                bindings[p.key.name] = __makeBindingObj(
                  p.value,
                  ns.makeAccessor('root', p.value),
                  ns.makeMutator('root', p.value)
                )
                cache_mark[p.key.name] = bindings[p.key.name].sig
        catch error
            console.log "[expr] Error while parsing binding #{str}"
            console.log error

        __BINDINGS_CACHE__[str] = cache_mark

        return bindings

    ns.getBindingExpression = (node)->
        return null if not node.attributes['data-bind']

        $n = window.jQuery(node)
        exprs = $n.data('_exprs')
        if exprs
            bindings = {}
            for k in Object.keys(exprs)
                bindings[k] = __BINDINGS__[exprs[k]]
            return bindings

        bindingExpr = $n.data('bind')
        bindingExpr = ns.parseBinding(bindingExpr)

        exprs = {}
        for k in Object.keys(bindingExpr)
            expr = bindingExpr[k]
            __BINDINGS__[expr.id] = expr
            exprs[k] = expr.id

        $n.data('_exprs', exprs)

        return bindingExpr

    # does actual validation on the binding expression
    # It gets the root object to validate against plus the ast tree
    # of the expression.
    #
    # It returns an array of errors
    ns.validateBindingExpr = (rootObject, node)->
        return []

    ns.disposeExpressions = (rootNode)->
        $r = window.jQuery(rootNode)
        for ch in $r.children()
            $ch = window.jQuery(ch)
            d = window.jQuery(ch).data()
            if d['_exprs']
                ex = d['_exprs']
                for k in Object.keys(ex)
                    bnd = __BINDINGS__[ex[k]]
                    delete __BINDINGS__[ex[k]]
                delete d['_exprs']
                window.jQuery(ch).data(d)

            ns.disposeExpressions(ch)

        d = $r.data()
        return if not d
        if d['_exprs']
            ex = d['_exprs']
            for k in Object.keys(ex)
                bnd = __BINDINGS__[ex[k]]
                delete __BINDINGS__[ex[k]]
            delete d['binding-exprs']
            $r.data('binding-exprs', d)


    return ns
