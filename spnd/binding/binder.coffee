define ['./expr', './handlers', './tracking'], (expr, handlers, tracking)->
    "use strict"

    BINDER = 0

    # Binder is the entry point for managing object binding
    class Binder
        constructor: (@root, @el, sync)->
            @id = BINDER++
            @async = not sync
            @__CONTEXTES__ = {}
            @__CON_COUNTER = 0
            @['settings'] = Binder.settings
            @__bind__()

        __bind__: =>
            @$e = $(@el)
            if @$e[0].dataset.bound
                throw new Error("Already bound")

            @$e.on('change.binder', @__valueChanged).data('bound', 1)
               .on('click.binder', 'input:checkbox', @__valueChanged)

            context = @__makeBindingContext(@root)
            n = @$e[0].firstChild
            while n
                @__bindNode(n, context) if n.nodeType == 1
                n = n.nextSibling

        __makeBindingContext: (root, parent)=>
            _binder = @
            _root = @root
            ctx = Object.create( {
                'makeContext': (child)->  _binder.__makeBindingContext(child, this)
                'toString': -> JSON.stringify({
                    'id':@id,
                    '$parent':if @$parent then @$parent.id else '-',
                    '$this':root
                })
            }, {
                'id':
                    enumerable: false, writable: false, configurable: false, value: (@__CON_COUNTER++).toString()
                'binder':
                    writable: false, value: @
                '$this':
                    writable: false, value: root
                '$parent':
                    writable: false, value: parent
            })
            #Object.seal(ctx)

            @__CONTEXTES__[ctx.id] = ctx
            return ctx

        __registerForUpdates: (node, ctx, binding, handler)=>
            accessor = ->  binding.fn.call(ctx, ctx.$this)
            tracking.monitor(ctx.$this, accessor, ()->
                handlers.applyHandler(handler, 'update', node, ctx, binding)
            )

        __applyExpression: (node, ctx, bindingExpr, doNotListen)=>
            for k in Object.keys(bindingExpr)
                expression = bindingExpr[k]

                if expression is null or typeof(expression)=="undefined"
                    throw new Error("Expression is empty?!")
                if not expression.fn or expression.fn==null
                    throw new Error("Expression function is null!")

                handlers.applyHandler(k, 'init', node, ctx, expression)
                if not doNotListen
                    #continue if bindingExpr['with']?
                    @__registerForUpdates(node, ctx, expression, k)


        __bindNode: (node, ctx, doNotListen)=>
            if node.nodeType != 1
                console.debug "Node type 1"
                return

            if node.dataset.bound
                return

            node.dataset.bound = true

            bindingExpr = expr.getBindingExpression(node)
            if bindingExpr
                node.dataset._ctx = ctx.id

                apply = =>@__applyExpression(node, ctx, bindingExpr, doNotListen)

                #do that after we finish
                if @async and (not bindingExpr['with']? and not bindingExpr['foreach']?)
                    setTimeout(apply, 2)
                else
                    apply()


            n = node.firstChild
            while n
                @__bindNode(n, ctx) if n.nodeType == 1
                n = n.nextSibling

        __valueChanged: (evt)=>
            $t = $(evt.target)
            bindings = expr.getBindingExpression($t[0])
            return if bindings == null

            ctx = @__CONTEXTES__[$t.data('_ctx')]
            for k in Object.keys(bindings)
                wfn = bindings[k].wfn

                #handler for value-bound elements
                if k == 'value'
                    wfn.call(ctx, ctx.$this, $t.val())

                #handler for checkboxes
                if k == "checked"
                    wfn.call(ctx, ctx.$this, $t[0].checked)

        __disposeElementBinding: (el, keepContext)=>
            n = el.firstChild
            while n
                @__disposeElementBinding(n, keepContext) if n.nodeType == 1
                n = n.nextSibling


            delete el.dataset.bound
            if el.dataset.__template?
                el.innerHTML = @__CONTEXTES__[el.dataset._ctx]
                delete el.dataset.__template

            if el.dataset?._ctx?
                ctx_id = el.dataset._ctx
                delete el.dataset._ctx
                ctx = @__CONTEXTES__[ctx_id]
                if ctx and not keepContext
                    @__CONTEXTES__[ctx_id] = null
                    tracking.disposeObject(ctx.$this) if ctx.$this['__monitor__']?.sig? > 0


        gc : =>
            for o in Object.keys(@__CONTEXTES__)
                delete @__CONTEXTES__[o] if @__CONTEXTES__[o]==null

        'dispose': ()=>
            @$e.removeData('bound')
              .off('change.binder')
              .off('click.binder')

            for r in @$e
                @__disposeElementBinding(r)
            expr.disposeExpressions(@object)
            @gc()

    Binder['settings'] = {

    }

    return Binder
