define ()->
    "use strict"
    # =======================================
    # Object changes and dependency tracking
    #
    # In order for the binding to be bi-directional we need notifications
    # on when an object is changed.
    #
    # To live-update on properties change we need a way to notify when an object
    # has actually changed.
    #
    # We do this by getting an object and transforming it in the following
    # way.
    #
    # We create a __monitor__ property that holds object and field metadata.
    # Each __monitor__ is assigned a unique id (called sig) that identifies
    # the object and never changes. The only way to change it is to clone
    # the object (as the __monitor__ property is not enumerable)
    #
    # For each property in the object, we replace it with a proxy that
    # delegates gets/sets to __monitor__ and optionally can call
    # a notify function for changes ( in case of set )
    #
    # For the getters, we can optionally (on existance) call a special function
    # (window.__monitor_get__) that gets notified when a getter is invoked
    # Then we can use that information to track the dependencies a property
    # or a function has from the monitored object.
    #
    # We can know then, that, when property x of an object changes
    # it's dependend properties will also be changed, so we need to re-render
    # the appropriate elements.
    # =======================================


    MONITORS = 0

    typeOf = (val)->
        return 'undefined' if typeof val == 'undefined'

        s = typeof val
        if s == 'object'
            if val
                if val instanceof Array
                    return 'array'
            else
                return 'null'

        return s

    TrackedArray = (array, __self, name) ->
        _this = new Array()
        _this.__artype__ = ''
        _this.push(i) for i in array

        _this.__push = _this.push;
        _this.push = (item)->
            _this.__push(item)
            if typeof(window.__monitor_set__) == "function" and typeof __self['__monitor__'] == "object"
                window.__monitor_set__(__self['__monitor__'][name])

        _this.empty = -> _this.length = 0

        return _this;

    __makeMetadataObj = (value, signature, parent, propName)->
        o = Object.create({'toString': ->return "#{@p}.#{@s}.#{@n}"},
        {
            'p':
                enumerable: false, writable: false, value: parent
            'n':
                enumerable: false, writable: false, value: propName
            's':
                enumerable: false, writable: false, value: signature
            'v':
                enumerable: false, writable: true, value: value
        }
        )
        Object.seal(o)
        return o

    __makeMonitored = (par, obj, parent)->
        return null if not obj?
        return obj if obj['__monitor__']?
        #if we have already seen it don't track it
        return if typeof obj != "object"
        #TODO: what happens on property addition ?

        parent or= 0
        msig = ++MONITORS

        # __monitor__ property is an internal property used for tracking object
        # values. Actually, it acts as a dictionary where the actual values
        # are stored, and in the original's properties place we set special
        # getters/setters doing our thing
        Object.defineProperty(obj, '__monitor__', {
            enumerable: false, configurable: false,
            value: Object.create(null, {
                'sig':
                    enumerable: false
                    writable: false
                    configurable: false
                    value: msig
            })
        })

        for k in Object.keys(obj)
            value = obj[k]
            continue if typeof value == "function"
            continue if value?.nodeType?
            continue if value?.disposeBindings
            continue if value and value.jquery?
            continue if k[0] == "_"
            continue if /jQuery/.test(k)

            if typeOf(value) is "array"
                obj['__monitor__'][k] = __makeMetadataObj(TrackedArray(value, obj, k ), msig, parent, k)
            else
                obj['__monitor__'][k] = __makeMetadataObj(value, msig, parent, k)

            if not obj['__model__'] #we need a special provision for models
                do (_self = obj, key = k)->
                    Object.defineProperty(obj, key, {
                        configurable: false,
                        get: ->
                            kmd = _self['__monitor__'][key]
                            window.__monitor_get__(kmd) if typeof window.__monitor_get__ == "function"
                            return kmd['v']

                        set: (nv)->
                            kmd = _self['__monitor__'][key]
                            if typeOf(nv) is 'array'
                                kmd['v'] = TrackedArray(nv, _self, key)
                            else
                                kmd['v'] = nv
                            window.__monitor_set__(kmd,
                              nv) if typeof window.__monitor_set__ == "function"
                    })


            if typeof value == 'object'
                if not (value instanceof Array or value instanceof Date)
                    __makeMonitored(obj, value, msig)

        return obj


    window.__TRACKED_EXPRESSIONS__ = {}
    #TODO: this should be separated to notification/execution
    #and maybe we should debounce the whole thing
    window['__monitor_set__'] = (kmd)->
        if window.__TRACKED_EXPRESSIONS__[kmd.s]?[kmd.n]?
            for fn in window.__TRACKED_EXPRESSIONS__[kmd.s][kmd.n]
                fn()


    # in order to track dependencies, we set a special function @ window
    # that is being called by the property GETTERS.
    __monitorExecution = (fn)->
        if typeof window['__monitor_get__'] == "function"
            throw new Error("monitor already attached")

        err = null
        deps = []
        window['__monitor_get__'] = (kmd)->
            deps.push(kmd)

        try
            fn()
        catch error
            err = error

        delete window['__monitor_get__']
        throw err if err

        return deps

    ## =================================
    ## MODULE EXPORTS
    ## =================================

    exports =

    # Monitors an fn bound to an object for modification and calls
    # the passed callback(cb) on change
        'monitor': (obj, fn, cb)->
            obj = __makeMonitored(null, obj)
            deps = __monitorExecution(fn)

            for d in deps
                if not window.__TRACKED_EXPRESSIONS__[d.s]
                    window.__TRACKED_EXPRESSIONS__[d.s] = {}
                    window.__TRACKED_EXPRESSIONS__[d.s][d.n] = []

                if not window.__TRACKED_EXPRESSIONS__[d.s][d.n]
                    window.__TRACKED_EXPRESSIONS__[d.s][d.n] = []


                if window.__TRACKED_EXPRESSIONS__[d.s][d.n].indexOf(cb) < 0
                    window.__TRACKED_EXPRESSIONS__[d.s][d.n].push(cb)

            return obj

    # Disposes all the expressions tracked for an object
    # Essentialy it cleans everything out
        'disposeObject': (obj)->
            if not @['isMonitored'](obj)
                console.debug "Object is not monitored"
                return
            if obj['__monitor__']? and typeof obj['__monitor__'] != "undefined"
                delete __TRACKED_EXPRESSIONS__[obj['__monitor__'].sig]
            null

    # Check if an object is monitored
        'isMonitored': (obj)-> return obj['__monitor__']?.sig? > 0

    return exports
