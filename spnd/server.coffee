define [], ()->
    __doRequest = (method, options)->
        $.extend(options, {
            'type': method,
        })

        return $.ajax options

    __normalizeArguments = (args, changes)->
        alen = args.length
        if alen == 2
            options = {
                'url': args[0],
                'data': args[1],
                'dataType': 'text',
                'headers': {}
            }

        if alen == 1
            if typeof(args[0]) == "string"
                options = {
                    'dataType': 'text',
                    'headers': {},
                    'url': args[0]
                }
            else
                options = args[0]

        if changes?.type
            options['dataType'] = changes.type

        return options

    __JSON = {
        'get': ()->
            return __doRequest('GET',
              __normalizeArguments(arguments, {'type': 'json'}))
        'post': ()->
            return __doRequest('POST',
              __normalizeArguments(arguments, {'type': 'json'}))
        'del': ()->
            return __doRequest('DELETE',
              __normalizeArguments(arguments, {'type': 'json'}))
        'put': ()->
            return __doRequest('PUT',
              __normalizeArguments(arguments, {'type': 'json'}))
    }

    __HTML = {
        'get': ()->
            return __doRequest('GET', __normalizeArguments(arguments))
        'post': ()->
            return __doRequest('POST', __normalizeArguments(arguments))
        'del': ()->
            return __doRequest('DELETE', __normalizeArguments(arguments))
        'put': ()->
            return __doRequest('PUT', __normalizeArguments(arguments))
    }

    SERVER = {
        'json': __JSON,
        'html': __HTML,
    }

    return SERVER
