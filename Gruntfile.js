module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-rev-package');
    grunt.loadNpmTasks('grunt-bumpx');
    grunt.loadNpmTasks('grunt-karma');

    grunt.initConfig({
        qunit: {
          files:['./spnd/tests/test_models.htm']
        },

        coffee: {
            glob_to_multiple: {
                expand: true,
                bare: true,
                cwd: '.',
                src: ['spnd/**/*.coffee'],
                dest: '.',
                ext: '.js'
            }
        },

        requirejs:{
            compile:{
                options:{
                    baseUrl:'.',
                    paths:{
                        'almond':'../../libs/almond'
                    },
                    out:'spnd.js',
                    name:'almond',
                    include:['spnd'],
                    exclude:["jquery"],
                    optimize:"none",

                    // code to wrap around the start / end of the resulting build file
                    // the global variable used to expose the API is defined here
                    wrap:{
                        start:"(function(global, define) {\n" +
                            // check for amd loader on global namespace
                            "  var globalDefine = global.define;\n",

                        end:"  var library = require('spnd');\n" +
                            "  if(typeof module !== 'undefined' && module.exports) {\n" +
                            // export library for node
                            "    module.exports = library;\n" +
                            "  } else if(globalDefine) {\n" +
                            // define library for global amd loader that is already present
                            "    (function (define) {\n" +
                            "      define(function () { return library; });\n" +
                            "    }(globalDefine));\n" +
                            "  } else {\n" +
                            // define library on global namespace for inline script loading
                            "    global['spnd'] = library;\n" +
                            "  }\n" +
                            "}(this));\n"
                    }
                }
            },
            compile2: {

                options:{
                    baseUrl:'.',
                    paths:{
                        'jquery':'bower_components/jquery/jquery'
                    },
                    out:'../spnd.dist.js',
                    name:'spnd',
                    exclude:["jquery"],
                    optimize:"none"
                }
            }
        },
        uglify: {
            spnd: {
                files: {
                    '../spnd.min.js':'../spnd.dist.js'
                }
            }
        },
        revPackage: {
            dist: '../spnd.min.js'
        },
        bump: {
            files: ['package.json']
        },
        karma: {
            options: {
                configFile: 'karma.conf.js',
                runnerPort: 9999
            },
            unit: {
                singleRun: true,
                browsers: ['PhantomJS']
            },
            dev: {
                browsers: ['Firefox'],
                reporters: 'dots'
            }
        }
    });


    grunt.registerTask('default', ['coffee','requirejs:compile2', 'bump::patch']);
    grunt.registerTask('release', ['coffee', 'karma:unit', 'requirejs:compile2', 'uglify', 'revPackage']);
    grunt.registerTask('almond', ['coffee','requirejs:compile']);
    grunt.registerTask('rebuild', ['coffee', 'requirejs:compile2', 'uglify', 'revPackage'])

}
